<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Balon extends Model
{
    protected $fillable = [
        'diametro',
        'volumen'
    ];
}
