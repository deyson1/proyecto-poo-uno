<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Cliente;

class ClienteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */



    public function InicioCliente(Request $request)
    {
        $Cliente = Cliente::all();
        return view('Cliente.inicio1')->with('Cliente', $Cliente);
    }

    public function CrearCliente (Request $request)
    
    {
        $Cliente = Cliente::all();
        return view('Cliente.crear1')->with('Cliente', $Cliente);


    }




    public function GuardarCliente(Request $request)
    {
        $this->validate($request, [
            'nombre' => 'required',
            'apellidos' => 'required',
            'cedula' => 'required',
            'dirección' => 'required',
            'teléfono' => 'required',
            'fecha_nacimiento' => 'required',
            'email' => 'required'
        ]);

        $producto = new Cliente;
        $producto->nombre = $request->nombre;
        $producto->apellidos = $request->apellidos;
        $producto->cedula = $request->cedula;
        $producto->dirección = $request->dirección;
        $producto->teléfono = $request->teléfono;
        $producto->fecha_nacimiento = $request->fecha_nacimiento;
        $producto->email = $request->email;
        $producto->save();
        
        return redirect()->route('list.cliente');
    }







    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
