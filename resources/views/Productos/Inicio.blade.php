@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">MODULO DE PRODUCTOS</div>

                <div class="col text-right">
                  <a href="{{ route('crear.productos') }}" class="btn btn-sm btn-primary">Nuevo producto</a>
                </div>

                <div class="card-body">
                <table class="table">
  <thead>
    <tr>
      <th scope="col"># ID</th>
      <th scope="col">NOMBRE</th>
      <th scope="col">TIPO</th>
      <th scope="col">ESTADO</th>
      <th scope="col">PRECIO</th>
    </tr>
  </thead>
  <tbody>
     @foreach ($producto as $item)
    <tr>
      <th scope="row">{{$item->id}}</th>
      <td>{{$item->Nombre}}</td>
      <td>{{$item->Tipo}}</td>
      <td>{{$item->Estado}}</td>
      <td>{{$item->Precio}}</td>
    </tr>
     @endforeach
    
  </tbody>
</table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

