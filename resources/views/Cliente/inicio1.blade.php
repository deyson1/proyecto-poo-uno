@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">CLIENTES</div>

                <div class="col text-right">
                  <a href="{{ route('crear.cliente') }}" class="btn btn-sm btn-primary">Nuevo Cliente</a>
                </div>

                <div class="card-body">
                <table class="table">
  <thead>
    <tr>
      <th scope="col"># ID</th>
      <th scope="col">NOMBRE</th>
      <th scope="col">APELLIDOS</th>
      <th scope="col">CEDULA</th>
      <th scope="col">DIRECCION</th>
      <th scope="col">TELEFONO</th>
      <th scope="col">FECHA DE NACIMIENTO</th>
      <th scope="col">EMAIL</th>
    </tr>
  </thead>
  <tbody>
     @foreach ($Cliente as $item)
    <tr>
      <th scope="row">{{$item->id}}</th>
      <td>{{$item->nombre}}</td>
      <td>{{$item->apellidos}}</td>
      <td>{{$item->cedula}}</td>
      <td>{{$item->dirección}}</td>
      <td>{{$item->teléfono}}</td>
      <td>{{$item->fecha_nacimiento}}</td>
      <td>{{$item->email}}</td>
    </tr>
     @endforeach
    
  </tbody>
</table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection