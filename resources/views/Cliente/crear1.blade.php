@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">CREAR CLIENTES</div>

                <div class="col text-right">
                  <a href="{{ route('list.cliente') }}" class="btn btn-sm btn-success">Cancelar</a>
                </div>

                <div class="card-body">

                <form role="form" method="POST" action="{{ route('guardar.cliente')}}">
                    {{csrf_field() }}
                    {{ method_field('post') }}

                    <div class="row">
                        
                        <div class="col-lg-4">
                        <label class="from-control-label" for="nombre">Nombre del cliente</label>
                        <input type="text" class="from-control" name="nombre">
                        </div>

                        <div class="col-lg-4">
                        <label class="from-control-label" for="apellidos">Apellidos</label>
                        <input type="text" class="from-control" name="apellidos">
                        </div>

                        <div class="col-lg-4">
                        <label class="from-control-label" for="cedula">Numero de cedula</label>
                        <input type="text" class="from-control" name="cedula">
                        </div>

                        <div class="col-lg-4">
                        <label class="from-control-label" for="dirección">Dirección</label>
                        <input type="text" class="from-control" name="dirección">
                        </div>

                        <div class="col-lg-4">
                        <label class="from-control-label" for="teléfono">Teléfono</label>
                        <input type="text" class="from-control" name="teléfono">
                        </div>

                        <div class="col-lg-4">
                        <label class="from-control-label" for="fecha_nacimiento">Fecha de nacimiento</label>
                        <input type="text" class="from-control" name="fecha_nacimiento">
                        </div>

                        <div class="col-lg-4">
                        <label class="from-control-label" for="email">Email</label>
                        <input type="text" class="from-control" name="email">
                        </div>

                    </div>
                    <button type="submit" class="btn btn-success pull-right" >Guardar</button>

                   </form>


                
                </div>
            </div>
        </div>
    </div>
</div>
@endsection