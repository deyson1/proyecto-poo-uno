<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

route::group(['middleware' =>['auth']], function() {

    route::post('/guardar/productos', ['as' => 'guardar.productos', 'uses'=>'ProductoController@GuardarProducto']);
    route::get('/lista/productos', ['as' => 'list.productos', 'uses'=>'ProductoController@InicioProducto']);
    route::get('/crear/productos', ['as' => 'crear.productos', 'uses'=>'ProductoController@CrearProducto']);

    route::post('/guardar/cliente', ['as' => 'guardar.cliente', 'uses'=>'ClienteController@GuardarCliente']);
    route::get('/lista/cliente', ['as' => 'list.cliente', 'uses'=>'ClienteController@InicioCliente']);
    route::get('/crear/cliente', ['as' => 'crear.cliente', 'uses'=>'ClienteController@CrearCliente']);

});